﻿using System;
using System.IO;

namespace AlternateCase

{
    // Ableitung von StreamWriter mit :
    public class AlternateCaseWriter : StreamWriter
    {
        
        // Konstruktorenverkettung 
        public AlternateCaseWriter(Stream stream) : base(stream)
        {

        }

        // String wird in die Write()Methode übergeben
        public override void Write(string value)
        {
            string alternateString = "";
            foreach (char character in value)
            {
                // Prüfung ob Buchstabe vorhanden
                if (!Char.IsLetter(character))
                {
                    alternateString += character;
                }
                else if (Char.IsLower(character))
                {
                    alternateString += Char.ToUpper(character);
                }
                else
                {
                    alternateString += Char.ToLower(character);
                }
            }
            base.Write(alternateString);
        }
    }
}
